import sqlite3


class Base:
    def __init__(self,db):
        self.connect = sqlite3.connect("Invoices.db")
        self.curs = self.connect.cursor()
        self.curs.execute("CREATE TABLE IF NOT EXISTS register (id INTEGER PRIMARY KEY, kontrahent text, numer integer, kwota integer, stawka integer)")
        self.connect.commit()

    def insertnew(self, kontrahent, numer, kwota, stawka):
        self.curs.execute("INSERT INTO register VALUES (NULL, ?,?,?,?)", (kontrahent, numer, kwota, stawka))
        self.connect.commit()

    def viewall(self):
        self.curs.execute("SELECT * FROM register")
        rows = self.curs.fetchall()
        return rows

    def search(self,kontrahent="", numer ="", kwota ="", stawka =""):
        self.curs.execute("SELECT * FROM register WHERE kontrahent=? OR numer =? OR kwota =? OR stawka=?", (kontrahent, numer, kwota, stawka))
        rows = self.curs.fetchall()
        return rows

    def delete(self,id):
        self.curs.execute("DELETE FROM register WHERE id=?", (id,))
        self.connect.commit()

    def update(self,id, kontrahent, numer, kwota, stawka):
        self.curs.execute("UPDATE register SET kontrahent=?, numer=?, kwota=?, stawka=? WHERE id=?", (kontrahent, numer, kwota, stawka,id))
        self.connect.commit()

    def __del__(self):
        self.connect.close()
