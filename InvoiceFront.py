from tkinter import *
from InvoiceBack import Base

dbase = Base("Invoices.db")
window = Tk()
window.wm_title("Biedny Księgowy")



def get_selected_row(event):

    global selected_tuple
    index = lb.curselection()[0]
    selected_tuple = lb.get(index)
    ent1.delete(0, END)
    ent1.insert(END,selected_tuple[1])
    ent2.delete(0, END)
    ent2.insert(END,selected_tuple[2])
    ent3.delete(0, END)
    ent3.insert(END,selected_tuple[3])
    ent4.delete(0, END)
    ent4.insert(END,selected_tuple[4])


def view():
    lb.delete(0, END)
    ent5.delete(0, END)
    for row in dbase.viewall():
        lb.insert(END, row)


def add():
    dbase.insertnew(kontrahent_txt.get(), numer_txt.get(), netto_txt.get(), stawka_txt.get())
    lb.delete(0, END)
    lb.insert(END, (kontrahent_txt.get(), numer_txt.get(), netto_txt.get(), stawka_txt.get()))


def update():
    dbase.update(selected_tuple[0], kontrahent_txt.get(), numer_txt.get(), netto_txt.get(), stawka_txt.get())


def search():
    lb.delete(0, END)
    for row in dbase.search(kontrahent_txt.get(), numer_txt.get(), netto_txt.get(), stawka_txt.get()):
        lb.insert(END, row)

def countgross():
    ent5.delete(0, END)
    ent5.insert(END, (selected_tuple[4] * selected_tuple[3]) / 100 + selected_tuple[3])

def delete():
    dbase.delete(selected_tuple[0])


l1 = Label(window, text="Kontrahent")
l2 = Label(window, text="Numer Faktury")
l3 = Label(window, text="Kwota Netto")
l4 = Label(window, text="Stawka VAT %")
l5 = Label(window, text="Kwota Brutto")

l1.grid(row=0, column=0)
l2.grid(row=1, column=0)
l3.grid(row=2, column=0)
l4.grid(row=3, column=0)
l5.grid(row=4, column=0)

kontrahent_txt = StringVar()
ent1 = Entry(window, textvariable = kontrahent_txt)
ent1.grid(row=0, column=1)


numer_txt = StringVar()
ent2 = Entry(window, textvariable = numer_txt)
ent2.grid(row=1, column=1)


netto_txt = StringVar()
ent3 = Entry(window, textvariable = netto_txt)
ent3.grid(row=2, column=1)


stawka_txt = StringVar()
ent4 = Entry(window, textvariable = stawka_txt)
ent4.grid(row=3, column=1)


brutto_txt = StringVar()
ent5 = Entry(window, textvariable = brutto_txt)
ent5.grid(row=4, column=1)

but1 = Button(window, text="Wszystkie", width=12, command=view)
but1.grid(row=0, column=3)

but2 = Button(window, text="Dodaj", width=12, command=add)
but2.grid(row=1, column=3)


but3 = Button(window, text="Aktualizuj", width=12, command=update)
but3.grid(row=2, column=3)


but4 = Button(window, text="Znajdź", width=12, command=search)
but4.grid(row=0, column=4)


but5 = Button(window, text="Usuń", width=12, command=delete)
but5.grid(row=1, column=4)


but5 = Button(window, text="Zakończ", width=12, command=window.destroy)
but5.grid(row=2, column=4)


but6 = Button(window, text="Oblicz brutto", width=12, command=countgross)
but6.grid(row=4, column=3)

lb = Listbox(window, height=6, width=60)
lb.grid(row=5, column=0, rowspan=6, columnspan=4)

scroll = Scrollbar(window)
scroll.grid(row=5, column=4, rowspan=6, columnspan=1)

lb.configure(yscrollcommand=scroll.set)
lb.bind('<<ListboxSelect>>', get_selected_row)
scroll.configure(command=lb.yview)

window.mainloop()